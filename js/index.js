layui.use(['element', 'laydate', 'form'], function () {
    var element = layui.element,
        form = layui.form,
        $ = layui.jquery,
        laydate = layui.laydate,
        mid = document.getElementById('mid'),
        num = 7,
        // type,
        // data,
        // iNum,
        id,
        heightArr = [],
        moveHeight;

    function scrollBlock() {
        $('.layui-tab-wrap p').css({left: ($('.layui-this').offset().left - 15)})
    }

    scrollBlock();
    $(window).resize(function () {
        scrollBlock();
    })
    $('.layui-tab-title li').click(function (index) {
        let tran = $(this).offset().left;
        console.log(tran);
        $('.layui-tab-wrap p').css({
            left: (tran - 15)
            // transform: 'translateX(170px)'
        })
    })

    laydate.render({
        elem: '#date' //指定元素
    });
    document.onselectstart = function () {
        return false;
    };

    /**
     *拖拽开始
     */
    $('.layui-form-item').on('dragstart', function (e) {
        // event.dataTransfer.setData("Text", event.target.id);
        e.originalEvent.dataTransfer.setData("Text", e.target.id);

    });

    /**
     *
     */
    function show() {
        $('.right-item').css({right: 0});
    }

    function hid() {
        $('.right-item').css({right: -335});
    }

    function addDom() {
        var html = '';
        html += `
                    <div class="radio">
                        <h4>选项</h4>
                        <div class="radio-item">
                            <p>
                                <input type="text" placeholder="输入选项标题">
                                <span class="del">-</span>
                                <span class="add">+</span>
                            </p>
                            <p>
                                <input type="text" placeholder="输入选项标题">
                                <span class="del">-</span>
                                <span class="add">+</span>
                            </p>
                            <p>
                                <input type="text" placeholder="输入选项标题">
                                <span class="del">-</span>
                                <span class="add">+</span>
                            </p>
                        </div>
                    </div>`
        $('.add-item').html(html);
    }

    function delAdd() {
        var leng = 3;
        $('.radio-item').on('click', '.del', function () {
            if (leng == 1) {
                $('.del').off('click');
                leng = 2;
            } else if (leng == 2) {
                $(this).parent().siblings().find('.del').css({border: '1px solid #e6e6e6', color: '#e6e6e6'});
                $(this).parent().remove();
            } else {
                $(this).parent().remove();
            }
            leng--;
            console.log(leng + '---');
        })
        $('.radio-item').on('click', '.add', function () {
            if (leng == 1) {
                $('.del').css({border: '1px solid #444', color: '#444'});
                var dom = $(this).parent().get(0).outerHTML;
                $(this).parent().after(dom);
            } else {
                var dom = $(this).parent().get(0).outerHTML;
                $(this).parent().after(dom);
            }
            leng++;
            console.log(leng + '+++');
        })
    }

    function title(id) {
        var type = $('#' + id).data('type');
        // console.log(type);
        console.log('#' + id);

        $('.right-item h2').text(type);
        $('.add-item').html('');
        $('.tips').html('');
        $('.btn-style').html('');

        if (type === '单选框' || type === '复选框' || type === '下拉选择框') {
            addDom();
            if (type === '下拉选择框') {
                $('.radio-item input').attr('placeholder', '输入下拉选项');
                $('.radio-item').on('blur', 'input', function () {
                    let valNum = 0;
                    $('#' + id).find('select').append('<option value="">' + $(this).val() + '</option>')
                    console.log($('#' + id).get(0));
                    form.render();
                })
            } else if (type === '单选框') {
                // var valArr = [];
                $('.radio-item').on('blur', 'input', function () {
                    // valArr.push($(this).val());
                    // console.log(valArr);
                    $('#' + id).children('div').append('<input type="radio" name="1" value="' + $(this).val() + '" title="' + $(this).val() + '" lay-verify="">')
                    console.log($('#' + id).get(0));
                    form.render();
                })
            } else if (type === '复选框') {
                $('.radio-item').on('blur', 'input', function () {
                    $('#' + id).children('div').append('<input type="checkbox" name="2" value="' + $(this).val() + '" title="' + $(this).val() + '"lay-skin="primary" lay-verify="">')
                    console.log($('#' + id).get(0));
                    form.render();
                })
            }

            delAdd();
        } else if (type == '单行输入框' || type == '文本框') {
            var html = '';
            html += `
                    <h4>提示文字 <span> 最多30字</span></h4>
                    <input type="text" placeholder="请输入" class="text">
                    `
            $('.tips').html(html);
        } else if (type === '按钮') {
            var html = '';
            html += `
                    <h4>样式:</h4>
                    <div class="layui-form"> 
                        <div class="layui-input-block">
                            <input type="radio" lay-filter='fil' name="color" value="1" title="墨绿" checked>
                            <input type="radio" lay-filter='fil' name="color" value="2" title="蓝色">
                            <input type="radio" lay-filter='fil' name="color" value="3" title="赤色">
                            <input type="radio" lay-filter='fil' name="color" value="4" title="橙色">
                            <input type="radio" lay-filter='fil' name="color" value="5" title="藏青">
                            <input type="radio" lay-filter='fil' name="color" value="6" title="雅黑">
                            <input type="radio" lay-filter='fil' name="color" value="7" title="银灰">
                        </div>
                    </div>
                    `
            $('.btn-style').html(html);
            form.render();
            // <label class="layui-form-label">背景色：</label>
            // console.log($(':radio[name="color"]:checked').val());
            form.on('radio(fil)', function (data) {
                console.log(data.othis[0].innerText.substring(1, 3)); //被点击的radio的value值
                console.log(data.value); //被点击的radio的value值
            });
        }
        // else if (type === '日期') {}

        $('.prompt input').val('');
        promptTitle(id, type);
    }

    var newid = [];

    function promptTitle(id, type) {
        newid.push(id);
        // console.log(newid);
        len = newid.length;

        $('.title').on('focus', function () {
            $(this).css({border: '1px solid rgb(49, 175, 233)'});
        })
        $('.title').on('blur', function () {
            console.log('#' + newid[len - 1]);
            $('#' + newid[len - 1]).children('label').text($(this).val());
            $(this).css({border: '1px solid #d2d2d2 '});
        });

        $('.tips').on('focus', 'input', function () {
            $(this).css({border: '1px solid rgb(49, 175, 233)'});
        })
        if (type === '文本框') {
            $('.tips').on('blur', 'input', function () {
                if ($(this).val() != '') {
                    $('#' + newid[len - 1]).find('textarea').attr('placeholder', $(this).val());
                }
                $(this).css({border: '1px solid #d2d2d2 '});
            })
        } else {
            $('.tips').on('blur', 'input', function () {
                if ($(this).val() != '') {
                    console.log('#' + newid[len - 1] + '====');
                    $('#' + newid[len - 1]).find('input').attr('placeholder', $(this).val());
                }
                $(this).css({border: '1px solid #d2d2d2 '});
            })
        }

        form.on('checkbox(requ)', function (data) {
            console.log(data.elem.checked + '====='); //是否被选中，true或者false
            if (data.elem.checked == true) {
                $('#' + newid[len - 1]).find('input').attr('lay-verify', 'required');
            }
        });
    }

    // function promptStyle(ele) {}
    function clickVal(ele) {
        var type = ele.data('type');
        $('.title').val($(ele).find('label').text());
        if (type === '文本框') {
            $('.tips input').val($(ele).find('textarea').attr('placeholder'));
        } else {
            $('.tips input').val($(ele).find('input').attr('placeholder'));
        }
    }

    function remove() {
        $('.mid-item .line').remove();
    }

    function move(leng, ele) {
        // let flag = true;
        for (var i = 0; i < leng; i++) {
            var iNum = [];
            iNum.push(i);
            // console.log(iNum);
            if (heightArr[i + 1] > moveHeight && moveHeight >= heightArr[i]) {
                // console.log(i);
                $('.mid-item .layui-form-item').eq(iNum[0]).after(ele);
                remove();
            } else if (0 <= moveHeight && moveHeight < heightArr[0]) {
                // if (flag) {
                $('.mid-item .layui-form-item').eq(0).before(ele);
                remove();
                // flag = false;
                // }
            } else if (moveHeight >= heightArr[leng - 1]) {
                // if (flag) {
                $('.mid-item .layui-form-item').eq(leng - 1).after(ele);
                remove();
                // flag = false;
                // }
            }
        }
    }

    /**
     *在目标元素中拖拽
     */
    mid.ondragover = function (e) {
        e.preventDefault();
        moveHeight = (e.clientY-70);
        // console.log(iNum);

        var leng = heightArr.length;
        // console.log(heightArr);
        // console.log(moveHeight);
        for (var i = 0; i < leng; i++) {
            var iNum = [];
            iNum.push(i);
            // console.log(iNum);
            if (heightArr[i + 1] > moveHeight && moveHeight >= heightArr[i]) {
                // console.log(i);
                remove();
                $('.mid-item .layui-form-item').eq(iNum[0]).after('<div class="line"></div>');
            } else if (0 <= moveHeight && moveHeight < heightArr[0]) {
                remove();
                $('.mid-item .layui-form-item').eq(0).before('<div class="line"></div>');
            } else if (moveHeight >= heightArr[leng - 1]) {
                remove();
                $('.mid-item .layui-form-item').eq(leng - 1).after('<div class="line"></div>');
            }
        }
    }

    /**
     *拖放
     */
    mid.ondrop = function (event) {
        show();
        var data = event.dataTransfer.getData("Text");
        num++;
        console.log(data);

        if (data.substring(4) < 8) {
            var leng = heightArr.length;
            // console.log(moveHeight);
            // console.log(heightArr);
            if (leng === 0) {
                $('.mid-item').append($('#' + data).clone().removeClass('item').css({width: '798px'}));
            } else {
                move(leng, $('#' + data).clone().removeClass('item').css({width: '798px'}));
            }

            // $('.mid-item').append($('#' + data).clone().removeClass('item').css({ width: '798px' }));
            $('.mid-item span').remove();
            $('.mid-item #' + data).attr('id', 'item' + num).children().removeClass('layui-hide').css({cursor: "move"});
            $('#item' + num).addClass('active').siblings('div').removeClass('active');
            remove();
            form.render();
            // console.log(num);
            id = 'item' + num;
            title(id);

            $(".mid-item .layui-form-item").click(function () {
                show();
                id = $(this).attr('id');
                $(this).addClass('active').siblings('div').removeClass('active');

                title(id);
                clickVal($(this));
            })

            heightArr = [];
            $(".mid-item .layui-form-item").each(function () {
                heightArr.push($(this).get(0).offsetTop);
            })

            $('#item' + num).on('dragstart', function (e) {
                e.originalEvent.dataTransfer.setData("Text", e.target.id);
                // e.originalEvent.dataTransfer.effectAllowed = 'move';
                // e.originalEvent.dataTransfer.dropEffect = 'move';
            })
        } else if (data.substring(4) >= 8) {
            console.log(data);
            $('#' + data).addClass('active').siblings('div').removeClass('active');
            title(data);
            var leng = heightArr.length;
            // console.log(heightArr);
            // console.log(moveHeight);

            move(leng, $('.mid-item #' + data));
        }

        /**
         *拖放离开目标元素
         */
        mid.ondragleave = function () {
            remove();
        }

        /**
         *
         */
        $(".mid-item .layui-form-item").hover(function () {
            var that = $(this);
            html = `<p>×</p>`;
            $(this).addClass('hoverActive').append(html);
            $('.mid-item p').css({
                width: 16, height: 16, fontSize: '18px', textAlign: 'center',
                lineHeight: '16px', position: 'absolute', top: 0, right: 0, cursor: 'pointer'
            })

            $('.mid-item p').click(function () {
                if (that.attr('id') == id) {
                    hid();
                }
                that.remove();

                heightArr = [];
                $(".mid-item .layui-form-item").each(function () {
                    heightArr.push($(this).offset().top);
                })
            })

        }, function () {
            $(this).removeClass('hoverActive');
            $('.mid-item p').remove();
        })

    }

    /**
     * 基础设置
     */
     $('.icon-click').click(function () {
          $('.icon-mask').css({display:'block'});
          $('.icon').show().addClass('layui-anim-upbit');
          // $('.icon').animate({display:'block', width: 600,height: 400},50);
     })
    $('.icon-mask').click(function () {
       $(this).css({display:'none'});
        $('.icon').hide();
    })

    /**
     * 高级设置  监听复选框单选框
     */
    form.on('checkbox(send-shenp)', function (data) {
        console.log(data.elem.checked);
    })
    form.on('checkbox(send-shenp)', function (data) {
        console.log(data.elem.checked);
    })
    form.on('checkbox(refer-to)', function (data) {
        if (data.elem.checked === true) {
            $('.card-span').removeClass('layui-hide');
            $('.tijiao').removeClass('layui-hide');
        } else if (data.elem.checked === false) {
            $('.card-span').addClass('layui-hide');
            $('.tijiao').addClass('layui-hide');
        }
    })

    form.on('checkbox(write)', function (data) {
        if (data.elem.checked === true) {
            $('.heWrite-box').removeClass('layui-hide');
        } else if (data.elem.checked === false) {
            $('.heWrite-box').addClass('layui-hide');
        }
    })


    /**
     * 取值
     */
    /*element.on('tab(tab)', function(data){
        console.log(data.index); //得到当前Tab的所在下标
        console.log(data.elem.get(0)); //得到当前的Tab大容器
        console.log($('.layui-tab-item').eq(data.index).get(0)) ;
        console.log(form.val('formVal'));
    });*/
    $('.issue').click(function () {
        var formVal1 = form.val('formVal1'),
            formVal2 = form.val('formVal2');

        console.log(formVal1);
        console.log(formVal2);
    })


});