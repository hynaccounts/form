layui.use(['element', 'laydate', 'form', 'layer'], function () {
    var element = layui.element,
        form = layui.form,
        layer = layui.layer,
        $ = layui.jquery,
        inpval;

    $('.typeOper').height($(document).height());

    /**
     *点击除元素以外的任意地方隐藏元素
     */
    $(document).click(function () {
        $('.sheji .add').hide();
        $('.wrap .item').removeClass('active');
    })

    var that;
    $('.wrap').on('click', '.line', function (event) {
        that = $(this);
        // 阻止冒泡
        var e = arguments.callee.caller.arguments[0] || event; //若省略此句，下面的e改为event，IE运行可以，但是其他浏览器就不兼容
        if (e && e.stopPropagation) {// this code is for Mozilla and Opera
            e.stopPropagation();
        } else if (window.event) {// this code is for IE
            window.event.cancelBubble = true;
        }
        $('.sheji .add').css({ top: $(this).offset().top, left: ($(this).offset().left + 35) }).show();
        $('.wrap .item').removeClass('cont');
    })


    function add(text, classEle, cId) {
        $('.sheji .add').on('click', classEle, function () {
            $(this).parent().hide();
            let dom = `
                <div class="item-box">
                    <div class="item" id="">
                        <input type="text" value="" class="inpTitle">
                    </div>
                    <div class="line">
                        <p>+</p>
                    </div>
                </div>
            `;

            if (that.parent().attr('class') === 'item-box left' || that.parent().attr('class') === 'item-box right') {
                that.after(dom);
                that.next().css({ marginTop: 15 })
                that.next().find('.item').addClass('cont').prop('id', id).prepend('<p class="error">×</p>');
                that.next().find('.inpTitle').val(text);
                that.next().siblings('.item-box').find('.item').removeClass('cont');

                that.next().find('.item').css({ width: 200 });

                inpval = that.next().find('.inpTitle').val();
                typeOpe(inpval, that.next(), cId);
            } else {
                that.parent().after(dom);
                that.parent().next().find('.item').addClass('cont').prop('id', cId).prepend('<p class="error">×</p>');//
                that.parent().next().find('.inpTitle').val(text);             //
                that.parent().next().siblings('.item-box').find('.item').removeClass('cont');

                inpval = that.parent().next().find('.inpTitle').val();
                typeOpe(inpval, that.parent().next(), cId);
            }

        });
    }

    function sP(value) {
        if (value === '指定成员') {
            let html2 = '';
            html2 += `
                    <button class="layui-btn layui-bg-blue">+ 添加成员</button>
                    <span>不能超过20人</span>
                    `;
            $('.poeOper').html(html2);
        } else if (value === '角色') {
            let html2 = '';
            html2 += `
                    <button class="layui-btn layui-bg-blue">+ 添加角色</button>
                    `;
            $('.poeOper').html(html2);
        } else if (value === '发起人自己') {
            let html2 = '';
            html2 += `
                    <p>发起人自己将作为审批人处理审批单</p>
                    `;
            $('.poeOper').html(html2);
        } else if (value === '发起人自选') {
            let html2 = '';
            html2 += `
                        <div class="layui-form-item mar">
                            <div class="layui-inline">
                                <select name="zSel1" id="shen-select">
                                    <option value="1">选项一</option>
                                    <option value="2">选项二</option>
                                    <option value="3">选项三</option>
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item mar layui-inline">
                            <label class="layui-form-label label">范围</label>
                            <select name="zSel2">
                                <option value="1">选项一</option>
                                <option value="2">选项二</option>
                                <option value="3">选项三</option>
                            </select>
                        </div>
                    `;
            $('.poeOper').html(html2);
            form.render();
        } else if (value === '表单联系人') {
            let html2 = '';
            html2 += `
                        <div class="layui-form-item mar">
                            <div class="layui-inline">
                                <select name="select" id="shen-select">
                                    <option value="1">选项一</option>
                                    <option value="2">选项二</option>
                                    <option value="3">选项三</option>
                                </select>
                            </div>
                        <b class='layui-icon layui-icon-tips'></b>
                        </div>
                    `;
            $('.poeOper').html(html2);
            form.render();
            $('.poeOper b').hover(function () {
                layer.tips('表单里的联系人控件可作为审批人', '.poeOper b', {
                    tips: [1, 'rgba(0,0,0,.6)'] //还可配置颜色
                });
            }, function () {
                layer.closeAll();
            })
        } else if (value === '连续多级主管') {
            let html2 = '';
            html2 += `
                        <p class='tit'>审批终点</p>
                        <div class='moreAdd'>
                            <input type="radio" lay-filter='fil' name="zhu" value="1" title="指定角色（同时是主管线上的主管）" checked>
                            <button class="layui-btn layui-bg-blue">+ 添加角色</button>
                        </div>
                        <input type="checkbox" lay-filter='che' id='che' name="che" value="同时不超过发起人向上的" title="同时不超过发起人向上的" class='child' lay-skin="primary">
                        <div class="layui-inline inpRig">
                            <select name="select1" id="shen-select1">
                                <option value="">请选择</option>
                                <option value="1">第一级</option>
                                <option value="2">第二级</option>
                                <option value="3">第三级</option>
                            </select>
                        </div>
                        <input type="radio" lay-filter='fil' name="zhu" value="2" title="通讯录中的">
                        <div class="layui-inline inpRig">
                            <select name="select2" id="shen-select2">
                                <option value="">请选择</option>
                                <option value="1">第一级</option>
                                <option value="2">第二级</option>
                                <option value="3">第三级</option>
                            </select>
                        </div>
                        <p class='tit'>审批人为空时</p>
                        <div class='layui-block'>
                            <input type="radio" lay-filter='sPeo' name="sPeop" value="自动通过" title="自动通过">
                            <input type="radio" lay-filter='sPeo' name="sPeop" value="自动转给管理员" title="自动转给管理员">
                        </div>
                    `;
            $('.poeOper').html(html2);

            form.render();
            form.on('radio(fil)', function (data) {
                if (data.value === '2') {
                    $('.child').attr('disabled', 'disabled').removeAttr('checked');
                    $('#shen-select1').attr('disabled', 'disabled');
                    $('.poeOper button').addClass('layui-hide');
                    form.render();
                } else if (data.value === '1') {
                    $('.child').removeAttr('disabled');
                    $('#shen-select1').removeAttr('disabled');
                    $('.poeOper button').removeClass('layui-hide').addClass('layui-show');
                    form.render();
                    form.on('checkbox(che)', function (data) {
                        console.log(data.elem.checked); //是否被选中，true或者false
                    });
                }
            })
            form.on('radio(sPeo)', function (data) {
                console.log(data.value);
            })
        } else if (value === '主管') {
            let html2 = '';
            html2 += `
                        <p class='tit'>主管</p>
                    `;
            $('.poeOper').html(html2);
        }
    }

    function typeOpe(inpval, ele, cId) {
        $('.start-item').html('');
        $('.shenpi-item').html('');
        $('.send-item').html('');
        $('.requirement-item').html('');

        $('.zheBox').show();
        $('.typeOper').show().animate({ right: 0 }, 'fast');
        $('.title h2').text(inpval);
        console.log(inpval);
        if (cId === 'shen') {
            // ele.find('.inpTitle').addClass('layui-bg-orange');
            let html = '';
            html += `
            <div class="layui-form">
                <p>设置审批人</p>
                <div class="layui-input-inline rad">
                    <input type="radio" lay-filter='fil' name="peo" value="指定成员" title="指定成员">
                    <input type="radio" lay-filter='fil' name="peo" value="主管" title="主管">
                    <input type="radio" lay-filter='fil' name="peo" value="发起人自己" title="发起人自己">
                    <input type="radio" lay-filter='fil' name="peo" value="角色" title="角色">
                    <input type="radio" lay-filter='fil' name="peo" value="发起人自选" title="发起人自选" checked>
                    <input type="radio" lay-filter='fil' name="peo" value="表单联系人" title="表单联系人">
                    <input type="radio" lay-filter='fil' name="peo" value="连续多级主管" title="连续多级主管">
                </div>
                <div class='poeOper'></div>
            </div>
            `;
            $('.shenpi-item').html(html);

            let html1 = '';
            html1 += `
                <div class="layui-form-item mar">
                    <div class="layui-inline">
                        <select name="select" id="shen-select">
                            <option value="1">选项一</option>
                            <option value="2">选项二</option>
                            <option value="3">选项三</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item mar layui-inline">
                    <label class="layui-form-label label">范围</label>
                    <select name="select">
                        <option value="1">选项一</option>
                        <option value="2">选项二</option>
                        <option value="3">选项三</option>
                    </select>
                </div>
                <p>多人审批时采用的审批方式</p>
                <div class="layui-form-item mar layui-input-block">
                    <input type="radio" value="依次审批" lay-filter='shP' name='sp' title="依次审批" checked>
                    <input type="radio" value="会签" lay-filter='shP' name='sp' title="会签">
                    <input type="radio" value="或签" lay-filter='shP' name='sp' title="或签">
                </div>
            `;
            $('.poeOper').html(html1);

            form.render();
            form.on('radio(fil)', function (data) {
                $('.rad').click(function () {
                    let value = $('.rad input:radio:checked').val();
                    sP(value);
                })
            })
            form.on('radio(shP)', function (data) {
                console.log(data.value); //被点击的radio的value值
            })

        } else if (cId === 'chao') {
            // ele.find('.inpTitle').addClass('layui-bg-blue');
            let html = '';
            html += `
            <p>${inpval}</p>
            <button class="layui-btn layui-bg-blue btn">添加成员</button>
            <br>
            <div class="layui-form-item">
                <input type="checkbox" value="允许发起人自选抄送人" title="允许发起人自选抄送人" lay-skin="primary" checked>
            <div/>
            `;
            $('.send-item').html(html);
            form.render();
        } else if (inpval === '发起人') {
            let html = '';
            html += `
                <p>谁可以提交</p>
                <input type="text" name="发起人" placeholder="所有人" value="" class='layui-input'>
            `;
            $('.start-item').html(html);
        } else if (cId === 'tiao') {
            let html = '';
            html += `
                 <div class="layui-form-item">
                     <label class="layui-form-label">发起人</label>
                     <div class="layui-input-inline">
                         <input type="text" class="layui-input" placeholder="请选择具体人员/角色/部门">
                     </div>
                 </div>
                 <button class="layui-btn">+ 添加条件</button>
            `
            $('.requirement-item').html(html);
        }
    }

    add('审批人', '.shenpi', 'shen');
    add('抄送人', '.send', 'chao');

    function each() {
        $('.requirebox').each(function (index, ele) {
            $('.requirebox').eq(0).css({ height: 280 * (index + 1) });
        });
    }

    function eachLeft(mar) {
        $('.left').each(function (index, ele) {
            $('.left').eq(0).css({ margin: '0 ' + (60 + mar) + 'px' });
        });
    }

    function eachRight(mar) {
        $('.right').each(function (index, ele) {
            console.log($('.right').eq(0).get(0))
            $('.right').eq(0).css({ margin: '0 ' + (60 + mar) + 'px' });
        });
    }

    var wid = 0,
        mar = 0;
    $('.sheji .add').on('click', '.requirement', function () {
        $(this).parent().hide();
        let dom = `
        <div class="requirebox">
            <div class="addIf">添加条件</div>
            <div class="add-dom">
                <div class="item-box left">
                    <div class="item">
                       <input type="text" value="条件1" class="inpTitle">
                    </div>
                    <div class="line">
                        <p>+</p>
                    </div>
                </div>
                <div class="item-box right">
                    <div class="item">
                       <input type="text" value="条件2" class="inpTitle">
                    </div>
                    <div class="line">
                        <p>+</p>
                    </div>
                </div>
            </div>
            <div class="line ifLine">
                <p>+</p>
            </div>
        </div>
        `

        if (that.parent().attr('class') === 'item-box left' || that.parent().attr('class') === 'item-box right') {
            that.after(dom);
            // .attr('data-type', '条件').text('条件')
            that.next().find('.item').addClass('cont').prop('id', 'tiao').prepend('<p class="error">×</p>');
            that.next().children().siblings('.item-box').find('.item').removeClass('cont');

            $('.requirebox .add-dom').eq(0).css({ width: (400 + (wid += 320)) });
            $('.requirebox .add-dom').addClass('add-pos').eq(0).removeClass('add-pos');
            // that.parent().css({margin: '0 80px'}).siblings('.right').css({margin: '0 80px'});

            that.next().find('.left').append(that.next().nextAll());

            eachLeft((mar += 20));
            // eachRight((mar+=20));

            each();
        } else {
            that.parent().after(dom);
            that.parent().next().find('.item').addClass('cont').prop('id', 'tiao').prepend('<p class="error">×</p>');
            that.parent().next().siblings('.item-box').find('.item').removeClass('cont');

            that.parent().next().find('.left').append(that.parent().next().nextAll());

            each();
        }

        $('.line').removeClass('last');
        $('.line:last').addClass('last');
    });

    $('.wrap').on('click', '.inpTitle', function (e) {
        e.stopPropagation();
        $(this).addClass('active');
    })
    $('.wrap').on('blur', '.inpTitle', function () {
        $(this).removeClass('active');
    })

    $('.wrap').on('click', '.addIf', function () {
        $(this).next().append($(this).next().children().eq(1).clone());

        let width = $(this).next('.add-dom').width();
        $(this).next('.add-dom').css({ width: (width + 210) });
    })

    $('.wrap').on('click', '.item', function (e) {
        e.stopPropagation();
        $(this).addClass('active').parent().siblings('.item-box').find('.item').removeClass('active cont');
        $('.sheji .add').hide();

        inpval = $(this).find('.inpTitle').val();
        let cId = $(this).prop('id');
        typeOpe(inpval, '', cId);

    });
    $('.item').eq(0).click(function () {
        inpval = $(this).find('.inpTitle').val();
        typeOpe(inpval);
    });
    form.on('submit(form)', function (data) {
        console.log(data.field);
        var data = { type: inpval, data: data.field };
        console.log(data);
        // console.log(data.field[Object.keys(data.field)[0]])
        /*for (let key in data.field){
            console.log(data.field[key]);
            console.log(key);
        }*/
        return false;
    });

    $('.esc').on('click', function () {
        // form.render();
        let data = form.val('type');
        console.log(data);
        $('.zheBox').hide();
        $('.typeOper').hide();
    })

    /**
     * 分类操作动画
     */
    $('.zheBox').click(function (e) {
        e.stopPropagation();
        $(this).hide();
        $('.typeOper').animate({ right: -400 }, 'fast');

    })
    // $('.typeOper').click(function (e) {
    //     // e.stopPropagation();
    //     layui.stope(e)
    // })

    /**
     *
     */
    $('.wrap').on('click', '.error', function (e) {
        e.stopPropagation();
        if ($(this).parents('.requirebox').get(0) != undefined) {
            // console.log($(this).parents('.requirebox').get(0));
            $(this).parents('.requirebox')[0].remove();
        } else if ($(this).parents('.requirebox').get(0) === undefined) {
            $(this).parents('.item-box').remove();
        }
        $('.line:last').addClass('last');
    })

    /**
     * 最后一个添加结束符
     */
    $('.line:last').addClass('last');

})